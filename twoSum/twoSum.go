package twosum

func twoSum(nums []int, target int) (res []int) {
	indexMap := make(map[int]int, len(nums))

	for i, num := range nums {
		if j, ok := indexMap[target-num]; ok {
			res = []int{j, i}
		}
		indexMap[num] = i
	}

	return res
}
